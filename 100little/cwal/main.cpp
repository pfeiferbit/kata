/**
 * @file main.cpp
 * @brief 100little - Count Words And Lines
 * @author Maximilian Pfeifer
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char* argv[]) {
    if (argc != 2) {
        cout << "USAGE: " << argv[0] << " filename";
    } else {
        ifstream file(argv[1]);
        if (!file.is_open()) {
            cout << "Error opening file " << argv[1] << "!";
        } else {
            unsigned int lines = 0,
                         words = 0;
            string line,
                   dump;
            for (lines; getline(file, line); lines++) {
                stringstream linestream(line);
                for (words; linestream >> dump; words++);
            }
            cout << "File " << argv[1] << " contains " << words
                 << " words in " << lines << " lines.";
        }
    }
    cout << endl;
    return 0;
}