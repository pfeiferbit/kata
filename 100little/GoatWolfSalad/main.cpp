/**
 * @file main.cpp
 * @brief 100little - Goat-Wolf-Salad
 * @author Maximilian Pfeifer
 */

#include <iostream>
#include <vector>
#include <string>

#include "gws.h"

using namespace std;

int main() {
    gws problem;
    vector<string> solution = problem.get_solution();
    cout << "Solution:" << endl << "{" << endl;
    for (auto it = solution.begin(); it != solution.end(); ++it) {
        cout << "\t" << *it << endl;
    }
    cout << "}" << endl;
    cin.get();
    return 0;
}