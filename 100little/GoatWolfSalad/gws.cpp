/**
 * @file gws.cpp
 * @brief 100little - Goat-Wolf-Salad
 * @author Maximilian Pfeifer
 */

#include "gws.h"

#include <iostream>
#include <vector>
#include <string>

gws::gws() {
    reset();
}

void gws::reset() {
    boat = goat = wolf = salad = left;
}

bool gws::take(position* gws) {
    if (gws) if (*gws != boat) return false;
    position from = boat;
    boat = river;
    if (gws) *gws = river;
    if ((goat == wolf || goat == salad) && goat != boat) {
        boat = from;
        if (gws) *gws = from;
        return false;
    }
    boat = from == right ? left : right;
    if (gws) *gws = boat;
    return true;
}

std::vector<std::string> gws::get_solution() {
    std::vector<std::string> solution;
    position* last = nullptr;
    while (boat != right || goat != right || wolf != right || salad != right) {
        if (last && take()) {
            last = nullptr;
            solution.push_back(std::string("take()"));
        } else if (last != &goat && take(&goat)) {
            last = &goat;
            solution.push_back(std::string("take(goat)"));
        } else if (last != &wolf && take(&wolf)) {
            last = &wolf;
            solution.push_back(std::string("take(wolf)"));
        } else if (last != &salad && take(&salad)) {
            last = &salad;
            solution.push_back(std::string("take(salad)"));
        }
    }
    reset();
    return solution;
}