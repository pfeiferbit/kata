/**
 * @file gws.h
 * @brief 100little - Goat-Wolf-Salad
 * @author Maximilian Pfeifer
 */

#ifndef GWS_H
#define GWS_H

#include <vector>
#include <string>

class gws {

private:
    enum position {
        left  = -1,
        river = 0,
        right = 1
    };
    position boat, goat, wolf, salad;
    
    void reset();
    bool take(position* gws = nullptr);

public:
    gws();

    std::vector<std::string> get_solution();

};

#endif