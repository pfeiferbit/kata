/**
 * @file main.cpp
 * @brief 100little - Roll The Dice
 * @author Maximilian Pfeifer
 */

#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

vector<int> rolls;

/* Keine C++ Multisets, sondern Multimengen aus der Kombinatorik */
int count_multisets(int n, int k) {
    int n_max = n + k,
        a = 1,
        b = 1;
    for (n; n < n_max; n++)
        a *= n;
    for (k; k > 1; k--)
        b *= k;
    return a / b;
}

/* Einen Wuerfel mit sides Seiten count mal rollen */
void roll_the_dice(int count, int sides) {
    for (int i = 0; i < count; i++) {
        rolls.push_back(rand() % sides + 1);
    }
}

/* Anzahl Multimengen durch Anzahl aller Kombinationen */
double get_probability(int sides) {
    return count_multisets(sides, rolls.size()) /
        pow(double(sides), double(rolls.size()));
}

/* Ausgabe der einzelnen Wuerfe */
void print_rolls() {
    for (auto it = rolls.begin(); it != rolls.end(); ++it) {
        cout << *it << " ";
    }
}

/* Abbruchausgabe */
int usage(char* name) {
    cout << "USAGE: " << name << " count [sides = 6]" << endl;
    return 1;
}

/* Einsprungspunkt */
int main(int argc, char* argv[]) {
    if (argc < 2 || argc > 3)
        return usage(argv[0]);
    char* end = nullptr;
    int count = strtol(argv[1], &end, 10);
    if (!end || *end)
        return usage(argv[0]);
    int sides;
    if (argc == 2)
        sides = 6;
    else {
        sides = strtol(argv[2], &end, 10);
        if (!end || *end)
            return usage(argv[0]);
    }
    srand(time(nullptr));
    cout << "Rolling " << count << " " << sides << "-sided dices ..." << endl;
    roll_the_dice(count, sides);
    print_rolls();
    cout << "(Probability: " << fixed << setprecision(3) << 
        get_probability(sides) << ")" << endl;
    return 0;
}