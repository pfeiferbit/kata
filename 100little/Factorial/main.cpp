/**
 * @file main.cpp
 * @brief 100little - Factorial
 * @author Maximilian Pfeifer
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/**
 * @brief Fakultaet rekursiv
 * @param n Eingabewert
 * @return unsigned int Ergebnis
 */
unsigned long int factorial_rec(unsigned long int n) {
    if (n == 0)
        return 1;
    return n * factorial_rec(n - 1);
}

/**
 * @brief Fakultaet iterativ
 * @param n Eingabewert
 * @return unsigned int Ergebnis
 */
unsigned long int factorial_it(unsigned long int n) {
    unsigned long int product = 1;
    for (n; n > 1; n--)
        product *= n;
    return product;
}

/**
 * @brief Fakultaet Fehlerbehandlung
 * @param n Eingabe (noch als C-String)
 * @param recursive Rekursive Berechnung, falls true
 * @return unsigned int Ergebnis
 */
unsigned long int factorial(char* n, bool recursive = false) {
    for (int i = 0; n[i] != 0; i++) {
        if (n[i] < '0' || n[i] > '9')
            return 0;
    }
    if (recursive)
        return factorial_rec(strtoul(n, nullptr, 0));
    return factorial_it(strtoul(n, nullptr, 0));
}

/**
 * @brief Ausgabefunktion
 * @param n Eingabe
 * @param result Ergebnis der Fakultaetsberechnung
 * @param recursive Rekursive Berechnung, falls true
 */
void printout(char* n, unsigned long int result, bool recursive = false) {
    cout << n << "! = ";
    if (result)
        cout << result;
    else
        cout << "undefined";
    if (recursive)
        cout << " (recursive)";
    cout << endl;
}

/**
 * @brief Einsprungspunkt mit Kommandozeilenparametern
 * @param argc Anzahl der Kommandozeilenparameter
 * @param argv Array der Kommandozeilenparameter als C-Strings
 * @return int Fehlercode
 */
int main(int argc, char* argv[]) {
    for (int i = 1; i < argc; i++) {
        unsigned long int f = factorial(argv[i]);
        unsigned long int f_rec = factorial(argv[i], true);
        printout(argv[i], f);
        printout(argv[i], f_rec, true);
    }
    return 0;
}