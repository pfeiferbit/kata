/**
 * @file main.cpp
 * @brief 100little - Reverse Strings
 * @author Maximilian Pfeifer
 */

#include <iostream>
#include <vector>
#include <string>

using namespace std;

const string END = "END";

int main() {
    vector<string> inputs;
    while (true) {
        string input;
        getline(cin, input);
        if (END.compare(input) == 0)
            break;
        inputs.push_back(input);
    }
    for (auto it = inputs.rbegin(); it != inputs.rend(); ++it) {
        cout << "-> " << *it << endl;
    }
    return 0;
}