Convert
=======

Convert konvertiert einen Betrag von einer Währung in eine andere.

Ziele
-----

- Funktionalität
- Fehlerbehandlung
- Extensibilität

Diese Ziele wurden alle erreicht. Weitere Währungen lassen sich bequem
eintragen, indem man die konstanten Werte am Programmanfang editiert.
Innerhalb des Programms kommen keine "magischen" Zahlen o.ä. vor, lediglich
die Indizes der Kommandozeilenparameter und die beschreibenden Texte bei der
Ausgabe sind fest einprogrammiert. Alle anderen Werte werden erst bei
Laufzeit ermittelt.
