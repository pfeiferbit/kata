/**
 * @file main.cpp
 * @brief 100little - Currency Converter
 * @author Maximilian Pfeifer
 */

#include <cstdlib>
#include <cstring>
#include <iostream>

using namespace std;

const int    CURRENCIES = 3;                      // Es gibt 3 Waehrungen
const char*  CURRENCY[] = {"USD", "EUR",  "BTC"}; // Dollar ist die Basis
const double COEFF[]    = {  1.0,  1.35, 631.18}; // 1 ABC = X USD


/* Umrechnungskurs ermitteln */
double rate(int id_source, int id_target) {
    return COEFF[id_source] / COEFF[id_target];
}

/* Wert umrechnen */
double conv(double value, int id_source, int id_target) {
    return value * rate(id_source, id_target);
}

/* ID -> ISO-4217 C-String */
const int get_id(char* code) {
    for (int i = 0; i < CURRENCIES; i++) {
        if (strcmp(code, CURRENCY[i]) == 0)
            return i;
    }
    return -1;
}

/* ISO-4217 C-String -> ID */
const char* get_code(int id) {
    if (id >= CURRENCIES || id < 0)
        return nullptr;
    return CURRENCY[id];
}

/* Hilfestellung */
int usage(char* name) {
    cout << "USAGE:   " << name << " [value] [source] [target]" << endl
         << "EXAMPLE: " << name << " 1.23    " << CURRENCY[0]
         << "      " << CURRENCY[1] << endl
         << endl
         << "List of available currencies:";
    for (int i = 0; i < CURRENCIES; i++) {
        cout << " " << CURRENCY[i];
    }
    cout << endl;
    return 1;
}

/* Einsprungspunkt */
int main(int argc, char* argv[]) {
    if (argc != 4)
        return usage(argv[0]);

    char* value_zombie = nullptr;
    double value = strtod(argv[1], &value_zombie);
    if (!value_zombie || *value_zombie)
        return usage(argv[0]);

    int id_source = get_id(argv[2]);
    if (id_source == -1)
        return usage(argv[0]);

    int id_target = get_id(argv[3]);
    if (id_target == -1)
        return usage(argv[0]);

    cout << value << " " << get_code(id_source) << " = "
         << conv(value, id_source, id_target) << " "
         << get_code(id_target) << " (Rate: 1 " << get_code(id_source)
         << " = " << rate(id_source, id_target) << " "
         << get_code(id_target) << ")" << endl;

    return 0;
}