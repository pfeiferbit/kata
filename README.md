Coding Katas
============

In den nächsten Tagen werde ich kleinere Coding Katas durchprogrammieren.

Die Aufgaben stammen von folgenden Quellen:

  1.  https://go-left.com/blog/programming/100-little-programming-exercises/
  2.  http://codingdojo.org/cgi-bin/wiki.pl?KataCatalogue
  3.  http://codekata.pragprog.com/2007/01/code_kata_backg.html#more

Erklärungen werden sich in den jeweiligen READMEs befinden. Fragen können über das Ticketsystem von Bitbucket oder persönlich per eMail/Facebook gestellt werden.

Falls nicht anders möglich (z.B. weil GPL-lizenzierte Bibliotheken verwendet wurden) oder vermerkt, wird alles in diesem Repository unter der MIT-Lizenz veröffentlicht.

Copyright (c) 2013 Maximilian Pfeifer
